# Mpecker

Bash/Shell script (launching FFmpeg) for generating a cut-up of an audio file.

A mathematical equation for a cycloid curve defines the position where a cut is made. The length of each chunk is 0.23 seconds. In total, 200 chunks are cut out. A new audio file is composed by following the order of the cutting process. The resulting file is ~46 seconds long.

Needs [FFmpeg](https://ffmpeg.org) installed.

Works only properly for **input** files that are **WAV files with a length of 10 seconds.**

## Instructions
- Create a project folder.
- Put the audio file you want to cut-up into this folder.
- But the script **mpecker.sh** into this folder, too.
- Make the .sh file executable.
- Open a Terminal (Command Line Interface), got to folder and run the script with `bash mpecker.sh`.
- **Enter filename:** e.g. `heart.wav`
- A new file will be generated with the prefix new, e.g. `new_heart.wav`. You’ll find it in your folder.


## Examples

![nervousdata - New Heart](nervousdata--new_heart--mpecker.mp3)

![nervousdata - New Face](nervousdata--new_face--mpecker.mp3)

