#!/bin/bash
# Mpecker

export LC_NUMERIC="en_US.UTF-8"

read -p "Enter filename: " fl
echo "File is: $fl"

ct=0
until [ $ct -gt 199 ] # sets how often a cut will be made (+1)
do
  ((ct+=1)) # a counter. starting at 1, increasing by 1
  ctt=`printf %03d $ct` # prints 3 digits numbers, important for naming the files
  zw=`awk -v x="$ct" 'BEGIN {wz=0.11*(0.41*x-4.0*sin(x))+0.5; printf "%.2f\n", wz}'` # defines a function for a cycloid curve (trochoid). "%.2f\n", rounds up to 2 digits after comma
  mkdir -p cuts # creates directories for the cut audio chunks
  echo " Cutting … $zw"
  ffmpeg -i $fl -acodec copy -ss 00:00:$zw -t 00:00:00.23 cuts/$ctt.wav # cuts the audio file. length of the snippet: 0.23 seconds; approx. eighth note in 130bpm track
done

for f in cuts/*.wav
 do
  echo "file '$f'" >> cuts.txt # creates a list of all files in the “cuts” folder
 done

while :
 do
  echo ' Assembling ! '
  ffmpeg -f concat -i cuts.txt -c copy new_$fl # composes a new audio file
  break
 done

rm -rf cuts/* # removes previous cutted files
rm cuts.txt # removes previous list
